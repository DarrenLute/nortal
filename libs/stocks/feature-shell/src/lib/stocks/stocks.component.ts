import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { debounceTime, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { PriceQueryFacade } from '@coding-challenge/stocks/data-access-price-query';


@Component({
  selector: 'coding-challenge-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})
export class StocksComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();

  stockPickerForm: FormGroup;
  symbol: string;
  period: string;
  datePickerMaxDate = new Date();
  isRange: boolean;

  quotes$ = this.priceQuery.priceQueries$;

  timePeriods = [
    { viewValue: 'All available data', value: 'max' },
    { viewValue: 'Five years', value: '5y' },
    { viewValue: 'Two years', value: '2y' },
    { viewValue: 'One year', value: '1y' },
    { viewValue: 'Year-to-date', value: 'ytd' },
    { viewValue: 'Six months', value: '6m' },
    { viewValue: 'Three months', value: '3m' },
    { viewValue: 'One month', value: '1m' }
  ];

  constructor(private fb: FormBuilder, public priceQuery: PriceQueryFacade) {
    this.stockPickerForm = fb.group({
      symbol: [null, Validators.required],
      period: [null],
      datePickerFromDate: [''],
      datePickerToDate: [new Date()]
    });
  }

  ngOnInit() {
    this.trackSymbolChange();
  }

  fetchQuote() {
    const formSymbol = this.stockPickerForm.get('symbol').value;
    const formPeriod = this.stockPickerForm.get('period').value;

    this.isRange = false;

    if (formSymbol && formPeriod) {
      this.priceQuery.fetchQuote(formSymbol, formPeriod);
    }
  }

  fetchQuoteRange() {
    const formSymbol = this.stockPickerForm.get('symbol').value;
    const datePickerFromDate = this.stockPickerForm.get('datePickerFromDate').value;
    const datePickerToDate = this.stockPickerForm.get('datePickerToDate').value;

    if (formSymbol && datePickerFromDate && datePickerToDate) {
      this.priceQuery.fetchQuoteRange(
        formSymbol,
        datePickerFromDate.toISOString().split('T')[0],
        datePickerToDate.toISOString().split('T')[0]
      );
    }
  }

  trackSymbolChange() {
    const tickerSymbol = this.stockPickerForm.get('symbol');

    tickerSymbol.valueChanges.pipe(
      debounceTime(1000),
      takeUntil(this.unsubscribe$)
    ).subscribe(() => {
      if (this.isRange) {
        this.fetchQuoteRange()
      } else if (!this.isRange) {
        this.fetchQuote();
      }
    })
  }

  checkRangeDates(isFromDate: boolean, isToDate: boolean) {
    const datePickerFromDate = this.stockPickerForm.get('datePickerFromDate').value;
    const datePickerToDate = this.stockPickerForm.get('datePickerToDate').value;

    this.isRange = true;

    if (datePickerFromDate >= datePickerToDate) {
      if (isFromDate) {
        this.stockPickerForm.controls['datePickerToDate'].setValue(datePickerFromDate);
      }
      if (isToDate) {
        this.stockPickerForm.controls['datePickerFromDate'].setValue(datePickerToDate);
      }
    }

    if (datePickerFromDate && datePickerToDate) {
      this.fetchQuoteRange();
    }
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
