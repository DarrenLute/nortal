import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import {
  MatDatepickerModule,
  MatFormFieldModule,
  MatInputModule,
  MatNativeDateModule,
  MatSelectModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';

import { GoogleChartsModule } from 'angular-google-charts';
import { StoreModule } from '@ngrx/store';

import { PriceQueryFacade } from '@coding-challenge/stocks/data-access-price-query';
import { StocksComponent } from './stocks.component';
import { ChartComponent } from '../../../../../shared/ui/chart/src/lib/chart/chart.component';


describe('StocksComponent', () => {
  let component: StocksComponent;
  let fixture: ComponentFixture<StocksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[
        BrowserAnimationsModule,
        ReactiveFormsModule,
        GoogleChartsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatDatepickerModule,
        MatNativeDateModule,
        StoreModule.forRoot({}),
      ],
      providers: [
        PriceQueryFacade
      ],
      declarations: [
        ChartComponent,
        StocksComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StocksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fetchQuote()', () => {
    const spy = spyOn(component.priceQuery, 'fetchQuote');
    component.stockPickerForm.controls['symbol'].setValue('tsla');
    component.stockPickerForm.controls['period'].setValue('max');

    component.fetchQuote();

    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('should fetchQuoteRange()', () => {
    const spy = spyOn(component.priceQuery, 'fetchQuoteRange');
    component.stockPickerForm.controls['symbol'].setValue('tsla');
    component.stockPickerForm.controls['datePickerFromDate'].setValue(new Date());
    component.stockPickerForm.controls['datePickerToDate'].setValue(new Date());

    component.fetchQuoteRange();

    expect(spy).toHaveBeenCalledTimes(1);
  });

  it('trackSymbolChange() should call fetchQuote() when isRange is false and the symbol entry is changed', <any>fakeAsync((): void => {
    const spy = spyOn(component, 'fetchQuote');
    component.isRange = false;

    component.stockPickerForm.controls['symbol'].setValue('tsla');
    tick(1100);

    expect(spy).toHaveBeenCalledTimes(1);
  }));

  it('trackSymbolChange() should call fetchQuoteRange() when isRange is true and the symbol entry is changed', <any>fakeAsync((): void => {
    const spy = spyOn(component, 'fetchQuoteRange');
    component.isRange = true;

    component.stockPickerForm.controls['symbol'].setValue('tsla');
    tick(1100);

    expect(spy).toHaveBeenCalledTimes(1);
  }));

  it('should checkRangeDates() from the from date picker and then fetchQuoteRange()', () => {
    const spy = spyOn(component, 'fetchQuoteRange');
    const fromDate = new Date(1995, 11, 17);
    const toDate = new Date(2020, 12, 12);
    component.stockPickerForm.controls['datePickerFromDate'].setValue(toDate);
    component.stockPickerForm.controls['datePickerToDate'].setValue(fromDate);

    component.checkRangeDates(true, false);

    expect(component.stockPickerForm.get('datePickerToDate').value).toEqual(toDate);
    expect(spy).toHaveBeenCalledTimes(1);

  });

  it('should checkRangeDates() from the to date picker and then fetchQuoteRange()', () => {
    const spy = spyOn(component, 'fetchQuoteRange');
    const fromDate = new Date(1995, 11, 17);
    const toDate = new Date(2020, 12, 12);
    component.stockPickerForm.controls['datePickerFromDate'].setValue(toDate);
    component.stockPickerForm.controls['datePickerToDate'].setValue(fromDate);

    component.checkRangeDates(false, true);

    expect(component.stockPickerForm.get('datePickerToDate').value).toEqual(fromDate);
    expect(spy).toHaveBeenCalledTimes(1);

  });
});
