const Hapi = require('@hapi/hapi');
const Wreck = require('@hapi/wreck');

const init = async () => {
  const server = Hapi.server({
    port: 3333,
    host: 'localhost'
  });

  server.route({
    method: 'GET',
    path: '/getStocks/{apiURL}/{symbol}/{period}/{apiKey}',
    handler: async (request, h) => {
      const { apiURL, symbol, period, apiKey } = request.params;
      const stockURL = `https://${apiURL}/beta/stock/${symbol}/chart/${period}?token=${apiKey}`;
      const { res, payload } = await Wreck.get(stockURL);

      return await payload.toString();
    },
    options: {
      cache: {
        expiresIn: 6 * 60 * 60 * 1000,
        privacy: 'private'
      },
      log: {
        collect: true
      }
    }
  });

  await server.start();
  console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {
  console.log(err);
  process.exit(1);
});

init();
